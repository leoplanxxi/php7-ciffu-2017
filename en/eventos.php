<?php require("header.php"); ?>



<div class="cabecera-articulo">
  <div class="row">
    <h2>Events 2017</h2>
  </div>
</div>

<!--
<div class="row tarjeta bloque-evento">
  <div class="small-12 medium-8 large-8 column text-justify">
    <h3>The Quantum Universe </h3>

    La jornada dedicada al “Mundo Cuántico”, se propone iniciar una serie de mini-talleres que cumplan con el propósito de fomentar la discusión de temas de frontera que involucren de manera significativa a la física cuántica. Esto con el objetivo de facilitar el trabajo de investigación en problemas básicos que jueguen un papel aglutinador de diversas disciplinas. El propósito del encuentro es identificar similitudes entre grupos diversos, con potenciales sinergias, para mirar los aspectos fundamentales de la física cuántica y sus posibles aplicaciones.
Para cumplir con este objetivo se propone convocar a expertos en dichos temas de investigación o afines, para que nos presenten pláticas de revisión general del tema o de sus propios avances.

    <br>
    <br>

            <h4>Ponentes invitados</h4>

            <ol>
              <li>Dr. Eduardo Gómez García (UASLP)</li>
              <li>Dra. Libertad Barrón Palos(UNAM)</li>
              <li>Dr. Sujoy Kumar Modak (UCOL)</li>
              <li>Dr. Juan Mauricio Torres González(BUAP)</li>
              <li>Dr. Francois Leyvraz Waltz (UNAM) </li>
            </ol>

                <br>
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSeJfCLCaTeswTYhIkJxRfu1G0ztDQo5JlXxWxRBY75aFVf_gA/viewform" target="_blank" class="button float-right">Inscribirse</a>

      <div class="row small-up-12 medium-up-2 large-up-2">
        <div class="column">
          <div class="lugar-evento">
            <div class="icono-evento">
            <img src="assets/iconos/clock-icon-dark.png">
            </div>

            <div class="contenido-bloque-evento">
              <h4>Cronograma de Actividades</h4>
              <p>
                <a href="archivos/The-Quantum-Universe-programaFIN.pdf">Programa 29‑30 junio 2017</a>
              </p>
            </div>

          </div>
        </div>
        <div class="column">
          <div class="lugar-evento">
            <div class="icono-evento">
            <img src="assets/iconos/clip-icon.png">
            </div>

            <div class="contenido-bloque-evento">
              <h4>Resumenes de pláticas</h4>
              <p>
                <a href="archivos/Abstract-QuantumUniverse.pdf">Resumen de ponencias</a>
              </p>
            </div>

          </div>
        </div>
      </div>

  </div>


  <div class="small-12 medium-4 large-4 column bloque-lugar">
    <div class="row">
          <div class="small-12 medium-6 large-6 column bloque-lugar">
            <h3>Lugar:</h3>

             Auditorio "Joaquín Ancona Albertos" 1FM3, FCFM

          </div>

          <div class="small-12 medium-6 large-6 column bloque-fecha">
            <h3>Fecha:</h3>

            29 y 30 de Junio del 2017
          </div>
    </div>


      <h3>Materiales:</h3>





      <a href="assets/img/seminarios/QUniversePoster_Difusion.jpg"><img src="assets/img/seminarios/QUniversePoster_Difusion.jpg"/></a>





  </div>





</div>
-->



<div class="row tarjeta bloque-evento">
  <div class="small-12 medium-8 large-8 column  text-justify ">
    <h3>Dark Matter Days</h3>


   After the discovery of the Higgs, the SM seems complete. However, some problems remain to be resolved.
Among others, the identification of the nature of Dark Matter rises as the next challenge to overcome.
In this workshop, we will have presentations of leading experts in this arena. Their work range from models of particle physics to classic proposals of gravity modification.
The objective of the workshop is twofold:<br><br>
• Transmit this advanced knowledge to students, postdocs and faculty members who are part of CIFFU-BUAP<br>
• Establish possible collaborations for future research projects on Dark Matter and Energy.<br>
The preliminar list of speakers includes: <br>
1.	Ernest Ma (UC-Riverside) : Dark matter models, Neutrino Physics<br>
2.	Roberto Lineros (Belgium): Connecting Neutrinos with light dark matter candidates,<br>
3.  Tonatih Matos (CINVESTAV): Bosonic consensate dark matter<br>
4.  Eduardo Peinado (IFUNAM) : Dark matter, Higgs and neutrinos<br>
5.  Mariana Vargas (IFUNAM) : Dark Energy Surveys<br>
6.  Eric Vazquez (IFUNAM) : Experimental searches for dark matter<br>
7.  Ana Avilez (CINVESTAV) : Scalar dark matter<br>
Short talks and poster session to show results of the students of our group on dark matter and related topics. 
<br><br>
Registration link:
<br>
<a href="https://indico.buap.mx/event/4/registrations/7/">https://indico.buap.mx/event/4/registrations/7/</a>
<br><small>Note: In case of you see the unprotected page sign, please choose the advanced version option.</small><br><br>

Program:<br>
<a href="http://www.ciffu.buap.mx/archivos/Dark-Matter-Days-Program-5nov17.pdf">PDF</a><br><br>

  </div>

  <div class="small-12 medium-4 large-4 column">
    <div class="row">
      <div class="small-12 medium-6 large-6 column bloque-lugar">
        <h3>Place:</h3>

        Auditorium "Joaquín Ancona Albertos" 1FM3, FCFM-BUAP
      </div>

      <div class="small-12 medium-6 large-6 column bloque-fecha">
        <h3>Date:</h3>
          November 6-8, 2017.

      </div>

    </div>

    <div class="row">
      <div class="small-12 column">
		 <!-- <a href="assets\img\CPW-Poster_Draft-01-001.jpg"><img src="assets\img\CPW-Poster_Draft-01-001.jpg"></a>-->
      </div>
    </div>
  </div>

</div>

<div class="row">
  <h2 class="header-style">Past events</h2>
</div>

<div class="row tarjeta">
  <ul class="accordion" data-accordion>
    <li class="accordion-item" data-accordion-item>
      <a href="#" class="accordion-title">Events 2017</a>

      <div class="accordion-content" data-tab-content>
          <table>
            <thead>
                <tr>
                  <th>Evento</th>
                  <th>Place</th>
                  <th>Date</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td>The Quantum Universe</td>
                <td>Auditorio "Joaquín Ancona Albertos" 1FM3, FCFM</td>
                <td>29 y 30 de Junio</td>
              </tr>
			  <tr>
                <td>CMS Day</td>
                <td>Auditorio "Joaquín Ancona Albertos" 1FM3, FCFM</td>
                <td>30 de Agosto</td>
              </tr>
			  <tr>
                <td>Collider Physics in Mexico</td>
                <td>Universidad Iberoamericana Ciudad de México</td>
                <td>From September 5th to 8th of 2017</td>
              </tr>
			  <tr>
                <td>COSMIC HIGGS CONNECTIONS (CHICO 17)</td>
                <td>Auditorio "Joaquín Ancona Albertos" 1FM3, FCFM</td>
                <td>27,28 Y 29 DE SEPTIEMBRE DE 2017</td>
              </tr>
			  


            </tbody>
          </table>

      </div>
    </li>


    <li class="accordion-item is-active" data-accordion-item>
      <a href="#" class="accordion-title">Events 2016</a>

      <div class="accordion-content" data-tab-content>
          <table>
            <thead>
                <tr>
                  <th>Event</th>
                  <th>Place</th>
                  <th>Date</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td>1st Workshop on Dark Matter</td>
                <td>Facultad de Ciencias Físico Matemáticas BUAP</td>
                <td>23-25 junio 2016</td>
              </tr>


            </tbody>
          </table>

      </div>
    </li>

    <li class="accordion-item" data-accordion-item>
      <a href="#" class="accordion-title">Events 2015</a>

      <div class="accordion-content" data-tab-content>
        <table>
          <thead>
              <tr>
                <th>Event</th>
                <th>Place</th>
                <th>Date</th>
              </tr>
          </thead>
          <tbody>
            <tr>

            </tr>


          </tbody>
        </table>
      </div>
    </li>
  </ul>


</div>

<?php require("footer.php"); ?>
